function [dist,err] = kendall_distance(sigma_gt, sigma)

% this code is from Basha et. al. ICCV 2013.

n = length(sigma);
n_gt = length(sigma_gt);

if(n < n_gt);
    sigma_gt = sigma_gt(sum(bsxfun(@ismember, sigma(:), sigma_gt),1)==1);
end

pairs_gt = nchoosek(sigma_gt, 2);
pairs_est = nchoosek(sigma, 2);

dist = size(setdiff(pairs_gt, pairs_est, 'rows'),1);
err = dist./size(pairs_est,1);

