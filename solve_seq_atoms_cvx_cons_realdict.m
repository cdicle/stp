function [P,c]= solve_seq_atoms_cvx_cons_realdict(S,D,theta,cons)

% S:  input sequence
% D:  dictionary
% theta: noise level
% cons: ordering constraints

% check if the we have any imaginary in the dictionary
if any(imag(D(:)))
    error('Dictionary must be all real.')
end

if nargin<4 || (nargin==4 && isempty(cons))
    % generate a random constraint for breaking the symmetry
%     cons = [1 2];
    cons = [];
end


[dim,len] = size(S);


if any(isnan(S(:)))
    
    error('Your data has NaN, this part is not ready yet.');
    % solve it with inpainting  
    B = isnan(S);
    
    % make nans 0
    S(B) = 0;
  
    cvx_clear    
    cvx_begin
    cvx_precision low
    cvx_solver gurobi
    
    % Permutation part
    variable P(len,len) binary
    sum(P,1)==1
    sum(P,2)==1
    % P = eye(len);
    
    X = S*P;
    
    % Fitting part
    cost = 0 ;
    variable c(size(D,2),dim)
    variable Z(dim,len)
    
    % Data fidelity constraints
    for i=1:dim
        real(D(1:end-1,:)*c(:,i)) == (Z(i,2:end)-Z(i,1:end-1))';
        imag(D(1:end-1,:)*c(:,i)) == 0;
        cost = cost + norm(c(:,i),1);
        abs(Z(i,:)-X(i,:)) <= theta + [double(B(i,:))*P]*1;
    end
    
    % order constraints
    L = [1:len];
    for n=1:size(cons,1)
        i = cons(n,1);
        j = cons(n,2);
        L*P(i,:)' < L*P(j,:)';
    end
     
    minimize(cost)
    
    cvx_end
    
else
    
    cvx_clear
    cvx_begin
    cvx_quiet true
    cvx_precision low
    cvx_solver gurobi
    
    % Permutation part
    variable P(len,len) binary
    sum(P,1)==1
    sum(P,2)==1
    % P = eye(len);
    
    X = S*P;
    
    % Fitting part
    cost = 0 ;
    variable c(size(D,2),dim) 
    variable Z(dim,len)
    
    for i=1:dim
        D(1:end-1,:)*c(:,i) == (Z(i,2:end)-Z(i,1:end-1))';
%         imag(D(1:end-1,:)*c(:,i)) == 0;
        cost = cost + norm(c(:,i),1);
        abs(Z(i,:)-X(i,:))<=theta;
    end
    
    % order constraints
    warning off
    L = [1:len];
    for n=1:size(cons,1)
        i = cons(n,1);
        j = cons(n,2);
        L*P(i,:)' < L*P(j,:)';
%        sum(L.*P(i,:)) < sum(L.*P(j,:));
    end
    warning on
    minimize(cost)
    
    cvx_end
end
P = round(P);
clear mex;
35;