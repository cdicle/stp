clear;close all;

rng(123123)
%% Test Setup
seqnames = {'cheetah','football','highjump','horjump','javelin','paper', ...
    'singer','skating','ski','soccer','sun','sylvester','vault','waves'}
diclepath = './data/';

for seqname={'ski'}
    
    seqname = seqname{1};
    fprintf('processing sequence %s\n',seqname);
    seqpath = [diclepath '/' seqname];
    saveseq = false;
    
    %% Load Data
    hseq = seqreader(seqpath);
    
    img0 = {};
    for t=1:hseq.NumOfFrames
        
        img = grabFrame(hseq);
        img0{t} = img;
        
        % resize
        img = imresize(img,0.20);
        gimg = rgb2gray(img);
        if t==1
            I = zeros(prod(size(gimg)),hseq.NumOfFrames);
        end
        I(:,t) = gimg(:);
        drawnow;
    end
    
    
    for iter=1:10
        fprintf('iteration %g ------->\n',iter);
        
        %% Generate A Permutation of the Data
        % Avoid the symmetry case. Keep the first frame in place, shuffle the rest.
        pind = [1 randperm(hseq.NumOfFrames-1)+1];
        J = I(:,pind);
        
        %% Recover The Original with Dicle et. al.
        seq_options.numOfBasis = 4;
        seq_options.maxDims = 10000;
        seq_options.tolerance  = 1e-2;
        seq_options.projectData = true;
        tic
        cons = [ones(1,size(J,2)-1);2:size(J,2)]'; % assume we know the first frame
        % [~,estorder,~] = solve_general_seq(J,[],seq_options,2);
        [~,estorder,~] = solve_general_seq(J,cons,seq_options,1);
        etime(iter,1) = toc;
        
        %% Compute the score
        gtorder = [1:size(J,2)];
        [dist,err] = kendall_distance(gtorder,pind(estorder));
        fprintf('dicle et. al. sequence: %s kendall_dist: %g runtime: %g secs\n',seqname, err,etime(iter,1));
        
        %% save
        if iter==1 && saveseq
            savepath = [diclepath '/' seqname '/output_dicle/'];
            mkdir(savepath);
            saveind = pind(estorder);
            for t=1:size(J,2)
                saveName = [savepath 'img' num2str(t,'%05g') '.jpg'];
                imwrite(img0{saveind(t)},saveName);
            end
        end
        
    end
end

