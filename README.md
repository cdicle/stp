# Solving Temporal Puzzles
*Caglayan Dicle, Burak Yilmaz, Octavia Camps, Mario Sznaier*


This is the official code repository for our [CVPR 2016 paper](http://www.cv-foundation.org/openaccess/content_cvpr_2016/papers/Dicle_Solving_Temporal_Puzzles_CVPR_2016_paper.pdf)

    @inproceedings{Dicle_2016_CVPR,
     author = {Dicle, Caglayan and Yilmaz, Burak and Camps, Octavia and Sznaier, Mario},
     title = {Solving Temporal Puzzles},
     booktitle = {The IEEE Conference on Computer Vision and Pattern Recognition (CVPR)},
     month = {June},
     year = {2016}
    }

### Disclamer
This code reproduces the results for video decryption part of the paper for proposed algorithm. The small differences are due to the random seed. Also original order *javelin* sequence is not correct. Please discard the results for that sequence. I left it anyways to keep the data part complete.

### How to run the STP code

 (1) Clone the repository
 
 (2) Run run_compare_dicleetal_data.m
	 	 		 	
### Prerequisites
The code was developed on MATLAB R2015b. You will need a compatible MATLAB version.
The solver uses [CVX 2.1](http://www.cvxr.com) and [Gurobi 6.5](http://www.gurobi.com)

### License
MIT License