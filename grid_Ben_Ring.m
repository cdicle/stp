% Function to create a dictionary of atomic responses as described in Ben
% Rechts paper on Linear Sys. ID using atomic norm minimization. 

% ro : Radius of the origin-centered disk gridded.
% delta_p: is the space between two consecutive poles in the real axis. 
% N : Number of time samples to consider in creating atomic impulses resp. 

% Output : D the dictionary, D_offset same dictionary shifted in both
% directions by delta_p. Poles are poles in D. 

% Author : Burak Yilmaz 
% Last Update : 03 May 2014. 


function [D, poles] = grid_Ben_Ring(delta_p,ro1,ro2,N)

 
xvec = -ro2:delta_p:ro2;
[x,y] = meshgrid(xvec,xvec);
mask = (x.^2 + y.^2 <= ro2^2) & (y>0) & (x.^2 + y.^2 >= ro1^2);
x = x(mask); y = y(mask);

poles = [x' + 1j*y' x'-1j*y'];
scalings = repmat(get_scalesBen(poles,N),[N 1]);
%scalings = ones(size(scalings));
Npoles = length(poles);
dummy = repmat(poles,[N-2 1]);
comp_vec = [zeros(1,Npoles);...
            ones(1,Npoles) ;...
            cumprod(dummy,1)];
D = comp_vec.*scalings;

%dummy = 1:N+1;
%nuc = sum(svd(hankel_mo(dummy)));

%D = [D (0:N-1)'/nuc];


% figure(100)
% plot(real(poles),imag(poles),'b*')
% axis('square')

end