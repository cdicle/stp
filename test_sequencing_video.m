clear;close all;
% addpath('~/research/code/matlab/common/seqreader');

% use seqreader
% seqpath = '~/research/data/sequencing/dicle/sun_around_earth'; skip=20; T=10;
% seqpath = '~/research/data/sequencing/dicle/paper_lower'; skip=3;T=10;
% seqpath = '~/research/data/sequencing/dicle/phantom_ski'; skip=8; T=10;
% seqpath = '~/research/data/sequencing/dicle/waves'; skip=3; T=10;

seqpath = '../data/dicleetal/cheetah'

saveseq = false;

hseq = seqreader(seqpath);
T = hseq.NumOfFrames
img0 = {};
for t=1:T
    img = grabFrame(hseq);
    img0{t} = img;
%     for r =1:skip
%         img = grabFrame(hseq);
%         img0{t} = img;
%     end
    % resize
    img = imresize(img,0.20);
    gimg = rgb2gray(img);
    if t==1
        I = zeros(prod(size(gimg)),T);
    end
    I(:,t) = gimg(:);
    imshow(img0{t});
%     imagesc(gimg);
    drawnow;
end

% compute the permutation from [2:end]
pind = randperm(T-1)+1;
% pind = randperm(T);
% J = I(:,pind);

%%

% TODO: Save original sequence in a folder too
if saveseq
    mkdir([seqpath '/input']);
    mkdir([seqpath '/output']);
    sind = [1 pind];
    for t=1:T    
        imwrite(img0{t},[seqpath '/input/img' num2str(t,'%05g') '.jpg'])
%         imwrite(img0{sind(t)},[seqpath '/input/img' num2str(t,'%05g') '.jpg'])
        imwrite(img0{t},[seqpath '/output/img' num2str(t,'%05g') '.jpg']) 
    end
    dlmwrite([seqpath '/input/perm.txt'],sind,'delimiter',' ');
end

% return;
%% compute the basis
[Iu,Is,~] = svd(I);
Is = diag(Is);
%% compute the projections onto the basis
% TODO: It might be a good idea to scale the frames
B = 5;
U = diag(1./Is(1:B))*Iu(:,1:B)'*I;

% for b=1:B
%     U(b,:) = U(b,:)/norm(U(b,:));
% end


%% compute the distance between frames
% D = zeros(T,T);
% for i=1:T
%     for j=i+1:T
%         dist = norm(J(:,i)-J(:,j));
%         D(i,j) = dist;
%         D(j,i) = dist;
%     end
% end




%% pick some pixels
% dim = 10;
% ind = randperm(size(I,1),dim);

%% form the data
% U = I(ind,:);
% U = mean(I,1);
% U = [linspace(1,T,T);linspace(T,1,T)];
% load U

S0 = U(:,1);
S  = U(:,pind);

%% check feasibility
theta = 1e-2;
% N = 12
% D = generate_dictionary(T,N);
% D = grid_Ben_Ring(0.0201,0.98,1.02,T-1);
D = grid_Ben_Ring(0.0501,0.98,1.02,T);

% augment D with flat and slope
d1 = ones(1,T)';   % N+1 because we have s0
d2 = linspace(1,T,T)';

D = [d1/norm(d1) d2/norm(d2) D];

%%
% NOTE: We are fitting to the derivative for better numerical behavior

cvx_begin
cvx_solver gurobi
cost = 0 ; 
variable c(size(D,2),size(U,1)) complex; 
variable Z(size(U,1),T) 

for i=1:size(U,1)
    real(D(1:end-1,:)*c(:,i)) == (Z(i,2:end)-Z(i,1:end-1))';
    imag(D(1:end-1,:)*c(:,i)) == 0; 
%     real(D*c(:,i)) == (Z(i,2:end)-Z(i,1:end-1))';
%     imag(D*c(:,i)) == 0; 
    cost = cost + norm(c(:,i),1);
    abs(Z(i,:)-U(i,:))<=theta;
end

% cost = cost + sum(abs(Z(i,2:end)-Z(i,1:end-1)))

minimize(cost) 

cvx_end



%% solve the problem

[P, chat] = solve_seq_atoms_cvx(S(2:4,:),S0(2:4,:),D,theta);
% [P,chat]= solve_seq_atoms_cvx(U(:,2:end),U(:,1),D,theta);
[1 sind(2:end)*P]