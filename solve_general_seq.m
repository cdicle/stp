function [sorteddata, estorder,chat ] = solve_general_seq(data,cons,options,mode)

% data: DxT D: data dimension T: number of instances
% options:
%       maxDims: Maximum number of dimensions
%       numOfBasis: Number of PCA dimensions to use. Not too much critical
%       tolerance: Approximation tolerance

data = double(data);

% default options
if nargin<2
    cons = [];
end

if nargin<3 || isempty(options)
    options.maxDims = inf;
    options.projectData = false;
    options.tolerance = 1e-1;
end

if nargin<4
    mode=1;
end
% If data is extremely high dimensional subsample it, so that svd does not
% take forever.
numDims = size(data,1);
if numDims > options.maxDims
    ind = randi(numDims,options.maxDims,1);
    data = data(ind,:);
end

if options.projectData
    % compute the projections onto the basis
    [U,D,~] = svd(data);
    D = diag(D);
    B = options.numOfBasis;  % number of basis. not too much critical but important
%     B = min(B,sum(D>1e-2));  % avoid very weak basis.
    S = diag(1./(D(1:B)+1e-2))*U(:,1:B)'*data;
    S1     = S(2:B,1);
    Srest  = S(2:B,2:end);
else
    S = data;
    S1     = S(:,1);
    Srest  = S(:,2:end);
end


% prepare the dictionary
T = size(data,2);
if 1
atom_dict = grid_Ben_Ring(0.0501,0.98,1.02,T);
atom_dict = [real(atom_dict) imag(atom_dict)];

% augment dictionary with flat and slope
% TODO: scale those correctly
d1 = ones(1,T)';
d2 = linspace(1,T,T)';
atom_dict = [d1/norm(d1) d2/norm(d2) atom_dict];
else
    atom_dict = grid_Ben_Ring(0.0501,0.98,1.02,T+1);
    atom_dict = [atom_dict(1:end-1,:) atom_dict(2:end,:)];
end




% remember that we kept the first element in place.
if mode==2
[P, chat] = solve_seq_atoms_cvx(Srest,S1,atom_dict,options.tolerance);
elseif mode==1
[P,chat] = solve_seq_atoms_cvx_cons_realdict(S,atom_dict,options.tolerance,cons);
elseif mode ==3
    [P,chat] = solve_seq_atoms_convex_cvx(S,atom_dict,options.tolerance,cons);
    
end

% find the permutation string from permutation matrix
[i,~]=find(round(P));
% estorder = [1 i'+1];
estorder = i';
sorteddata = data*P;
35;


