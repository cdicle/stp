function scale = get_scalesBen(poles,N_hor)

 if mod(N_hor,2) == 0
    N_odd =  2*ceil(N_hor/2)-1;   % Nearest odd number <= N, so that Hankel is square
    L = (N_odd+1)/2;              % Dimension of the square hankel matrix
 else
    N_odd =  2*ceil(N_hor/2)-1;   % Nearest odd number <= N, so that Hankel is square
    L = (N_odd+1)/2-1;              % Dimension of the square hankel matrix
 end
    
 

    scale = (1-abs(poles).^2)./(1-abs(poles).^(2*L));

end